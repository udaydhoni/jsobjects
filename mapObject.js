function mapObject (object, callback) {

    let newObject = {}

    for (let properties in object) {
        newObject[properties] = callback(object[properties],properties);
    }

    return newObject;

}

module.exports = mapObject;