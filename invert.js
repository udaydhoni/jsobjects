function invert(object) {

    let invertedObject = {};

    for (let keys in object) {
        invertedObject[object[keys]] = keys;
    }

    return invertedObject;
}

module.exports = invert;