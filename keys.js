function keys(object) {
    let result = [];

    for (let properties in object) {
        result.push(properties.toString());
    }

    return result;
}

module.exports = keys;
