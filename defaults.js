function defaults(object,defaultproperties) {
    for (let properties in defaultproperties) {
        if (object.hasOwnProperty(properties) === false) {
            object[properties] = defaultproperties[properties];
        }
    }
    return object;
}

module.exports = defaults;
